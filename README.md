
# OCTI MOXI
## Show us some cool tricks!

In this folder is a very basic ios app that loads a video of a human figure along with some  data about the joint locations at each time frame.
Your challenge is to use your SceneKit + Swift graphics programming skills to add some custom visual effects that use the animating joint positions.  What you create is completely up to you, but we suggest you don't spend more than a few hours on it.

## Specific Instructions:

1 - Open Octi-moxi.xcworkspace in xcode 9 beta.  
2 - Run the software on your phone or in an emulator.  You should see a looping video of one of us with some spheres drawn on our joints  
3 - Examine AnimationManager.swift.  Note that `setup`, `doAnimation` and `reset` is where you will likely be injecting most of your code.  


## Some ideas to get you started:

1 - Build some particle trails that follow our arms and legs.  
2 - Can you think of a way to write a custom shader for the AVPlayer?  
3 - Make some fun dynamic geometry.  
4 - Be yourself!  



## Joints

Below you can find which index in each joints ``.positions`` array corresponds to which joint in the body.

```
0 - root
1 - left hip
2 - left knee
3 - left ankle
4 - right hip
5 - right knee
6 - right ankle
7 - neck
8 - head
9 - left shoulder
10 - left elbow
11 - left wrist
12 - right shoulder
13 - right elbow
14 - right wrist
```

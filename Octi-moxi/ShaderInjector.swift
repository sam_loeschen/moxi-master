//
//  ShaderInjector.swift
//  Octi-moxi
//
//  Created by Sam on 11/8/17.
//  Copyright © 2017 Octi. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

class ShaderInjector{
    var metalView: MetalView!
    
    //dictionary defining pixel buffer format
    lazy var playerItemVideoOutput: AVPlayerItemVideoOutput = {
        let attributes = [kCVPixelBufferPixelFormatTypeKey as String : Int(kCVPixelFormatType_32BGRA)]
        return AVPlayerItemVideoOutput(pixelBufferAttributes: attributes)
    }()
    
    //display link updates pixel buffer each frame
    lazy var displayLink : CADisplayLink = {
        let displaylink = CADisplayLink(target : self, selector: #selector(readBuffer))
        displaylink.add(to: .current, forMode: .defaultRunLoopMode)
        displaylink.isPaused = true //start link paused
        return displaylink
    }()
    
    func attachToPlayer(avPlayer: AVPlayer){
        let playerItem = avPlayer.currentItem!
        playerItem.add(playerItemVideoOutput)
        avPlayer.replaceCurrentItem(with: playerItem)
    }
    
    func resumeDisplayLink(){
        displayLink.isPaused = false
    }
    
@objc func readBuffer(displayLink: CADisplayLink){
        var currentTime = kCMTimeInvalid
        let nextVSync = displayLink.timestamp + displayLink.duration
        currentTime = playerItemVideoOutput.itemTime(forHostTime: nextVSync)
        if playerItemVideoOutput.hasNewPixelBuffer(forItemTime: currentTime), let pixelBuffer = playerItemVideoOutput.copyPixelBuffer(forItemTime: currentTime, itemTimeForDisplay: nil){
                self.metalView.pixelBuffer = pixelBuffer
                self.metalView.inputTime = currentTime.seconds
        }
    }
}

//
//  ColorKernel.metal
//  Octi-moxi
//
//  Created by Sam on 11/8/17.
//  Copyright © 2017 Octi. All rights reserved.
//
#include <metal_stdlib>
using namespace metal;


// 8x8 bayer ordered dithering pattern. each input pixel is scaled to the 0..63 range before
//looking in this table to determine luminance
constant int dither[8][8] = {
    { 0, 32, 8, 40, 2, 34, 10, 42},
    {48, 16, 56, 24, 50, 18, 58, 26},
    {12, 44, 4, 36, 14, 46, 6, 38},
    {60, 28, 52, 20, 62, 30, 54, 22},
    { 3, 35, 11, 43, 1, 33, 9, 41},
    {51, 19, 59, 27, 49, 17, 57, 25},
    {15, 47, 7, 39, 13, 45, 5, 37},
    {63, 31, 55, 23, 61, 29, 53, 21}
};

float find_closest(int x, int y, float c0){
    half limit = x < 8 ? (dither[x][y] + 1) / 64.0 : 0.0;
    return c0 < limit ? 0.0 : 1.0;
}
kernel void colorKernel(texture2d<float, access::read> inTexture [[ texture(0) ]],
                        texture2d<float, access::write> outTexture [[ texture(1) ]],
                        device const float *time [[ buffer(0) ]],
                        uint2 gid [[ thread_position_in_grid ]])
{
    
    float Scale = 0.25;
    
    float4 lum = float4(0.299, 0.587, 0.114, 0); //standard rgb luminance
    float grayscale = dot(inTexture.read(gid), lum);
    
    float2 xy = float2(gid.x * Scale, gid.y * Scale);
    int x = int(fmod(xy.x, 8));
    int y = int(fmod(xy.y, 8));
    
    float final = find_closest(x, y, grayscale);
    float4 colorAtPixel = float4(final, final, final, 1.0);
    
    outTexture.write(colorAtPixel, gid);
}

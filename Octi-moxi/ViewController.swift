//
//  ViewController.swift
//  Octi-moxi
//
//  Created by Octi on 9/1/17.
//  Copyright © 2017 Octi. All rights reserved.
//

import UIKit
import SceneKit
import AVFoundation
import MetalKit
import SwiftyJSON


class ViewController: UIViewController {

    // Views
    @IBOutlet weak var scnView: SCNView!
    @IBOutlet weak var metalView: MetalView!
    //Sam: Shader Injector, moved player out of PlayerView as that is replaced with MetalView
    var shaderInjector : ShaderInjector!

    var player : AVPlayer!

    // Scene
    var scnScene: SCNScene!
    var cameraNode: SCNNode!

    // Window
    var origin = SCNVector3Zero
    var topLeft = SCNVector3Zero
    var bottomRight = SCNVector3Zero
    var worldSize : Vector2 = .zero
    
    // Data
    var asset: AVAsset!
    let videofile = "File_006"
    let videosize = CGSize (width: 1080, height: 1920)
    let videofps : Double = 30.0
    var jointFrames : [Joints] = []
    var gestureFrames : [Gesture] = []
    
    // Time
    var videoDuration: Double = 0
    var currentFrame : Int = 0
    var totalFrames : Int = 0
    
    // Animation Manager
    let animationManager = AnimationManager ()
    
    func setupScene () {
        scnScene = SCNScene()
        scnView.scene = scnScene
        
        cameraNode = SCNNode()
        let camera = SCNCamera()
        cameraNode.camera = camera
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 20)
        
        // JSON data assumes an orthographic camera
        camera.usesOrthographicProjection = true
        camera.orthographicScale = 1
        
        scnView.pointOfView = cameraNode
        scnView.allowsCameraControl = false
        scnView.backgroundColor = .clear
        
        //Sam: just going to init the shader injector here
        shaderInjector = ShaderInjector()
        shaderInjector.metalView = self.metalView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScene()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear (_ animated: Bool) {
        // Video Loading
        guard let videoURL = Bundle.main.url(forResource: self.videofile, withExtension: "mov") else {
            return assert(false, "Could not get bundle url for video")
        }
        
        self.asset = AVURLAsset(url: videoURL, options: [AVURLAssetPreferPreciseDurationAndTimingKey: true])
        

        //Sam: we don't need PlayerView anymore, so set up the AVPlayer here and attach
        //the shader injector
        let playerItem = AVPlayerItem(asset: asset)
        self.player = AVPlayer(playerItem: playerItem)
        shaderInjector.attachToPlayer(avPlayer: self.player)
        self.videoDuration = playerItem.asset.duration.seconds
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        pausePlayback()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.origin = self.scnView.projectPoint(SCNVector3Zero)
        self.topLeft = self.scnView.unprojectPoint(SCNVector3Make(0, 0, self.origin.z))
        self.bottomRight = self.scnView.unprojectPoint(SCNVector3Make(Float(self.scnView.bounds.width), Float(self.scnView.bounds.height), origin.z))
        self.worldSize = Vector2(self.bottomRight.x - self.topLeft.x, self.topLeft.y - self.bottomRight.y)
        assert(self.worldSize.y > 0)
        
        animationManager.addToController (viewController: self)
        parseFile ()
        resumePlayback ()
    }

    // Parse the JSON data and feed to animationManager
    func parseFile () {
        //Get JSON Data
        guard let demoFramesURL = Bundle.main.url(forResource: "\(videofile).mov", withExtension: "json"),
            let string = try? String(contentsOf: demoFramesURL),
            let animationsJSON = JSON(parseJSON: string)["animations"].array,
            let framesJSON = JSON(parseJSON: string)["framesData"].array
            else {
                fatalError()
            }
        
        let frames = framesJSON.map { frame -> RawJoints in
            let xy = frame["xy"].arrayValue.map { $0.arrayValue.map { $0.floatValue } }
            let joints = frame["joints"].arrayValue.map { $0.arrayValue.map { $0.floatValue }}
            let index = frame["index"].intValue
            let time = self.videoDuration * Double (index) / Double(framesJSON.count)
        
            return RawJoints(time: time, meanX: frame["meanX"].floatValue, meanY: frame["meanY"].floatValue, scale: frame["scale"].floatValue, xy: xy, joints: joints)
        }
        
        //Re-map to world space
        self.jointFrames = frames.map { rawFrame -> Joints in
            return Joints(rawJoints: rawFrame,
                          worldTopLeft: self.topLeft,
                          worldBottomRight: self.bottomRight,
                          videoSize: self.videosize)
        }
        
        self.gestureFrames = animationsJSON.filter({ $0["type"].string == "dab" }).map { animationFrame in
            let fn = animationFrame["frame_id"].intValue
            let t = self.videoDuration * Double (fn) / Double (jointFrames.count)
            return Gesture(frameNumber: fn, time: t)
        }
        
        animationManager.load (jointFrames: jointFrames, gestureFrames: gestureFrames)
    }
    
    
    var timeObserver: Any?
    func resumePlayback() {
        //Sam: resume display link in shader injector
        shaderInjector.resumeDisplayLink()
        
        // Play the video
        player.play()
        
        // Invoke callback pretty often, it's cheap
        let interval = CMTime(seconds: 1.0 / (4.0 * videofps),
                              preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        // Queue on which to invoke the callback
        let mainQueue = DispatchQueue.main
        // Add time observer to the player itself
        self.timeObserver = player.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue){ [weak self]
            (time: CMTime) -> Void in
            let secs = Double ( time.value ) /  Double( time.timescale)
            self?.animCallback (time: secs)
            
            // loop video
            let player = self!.player!
            let epsilon = 0.01
            if time.seconds > player.currentItem!.duration.seconds - epsilon {
                player.pause()
                player.seek(to: kCMTimeZero)
                player.play()
                
                self?.resetManager()
            }
        }
    }
    
    func animCallback (time: Double) {
        animationManager.doAnimation (time: time)
    }
    
    func pausePlayback() {
        if let timeObserver = self.timeObserver {
            self.player.removeTimeObserver(timeObserver)
        }
        
        self.player.pause()
        self.player.seek(to: kCMTimeZero)
        self.resetManager()
    }
    
    func getTime (atFrame frame: Int) -> Double {
        return Double (frame) / Double (self.totalFrames)
    }
    
    func resetManager (){
        animationManager.reset()
    }
}


//
//  FrameData.swift
//  Octi-moxi
//
//  Created by Octi on 9/1/17.
//  Copyright © 2017 Octi. All rights reserved.
//

import Foundation
import SwiftyJSON
import SceneKit

struct Gesture {
    let frameNumber: Int
    let time: Double
}

struct RawJoints {
    let time: Double
    let meanX: Float
    let meanY: Float
    let scale: Float
    let xy: [[Float]]
    let joints: [[Float]]
}

struct Joints {
    let raw: RawJoints
    let positions: [Vector3]
    
    init(rawJoints: RawJoints, worldTopLeft topLeft: SCNVector3, worldBottomRight bottomRight: SCNVector3, videoSize: CGSize) {
        self.raw = rawJoints
        
        let worldSize = Vector2(bottomRight.x - topLeft.x, topLeft.y - bottomRight.y)
        assert(worldSize.y > 0)
        
        // Re-project to image space & flip axes for RHCS
        let tjointpos = rawJoints.joints.map { Vector3($0[0] * rawJoints.scale+rawJoints.meanX, -($0[1] * rawJoints.scale + rawJoints.meanY), -$0[2] * rawJoints.scale) }
        
        // Align world coordinates with video coordinates
        let ratio = worldSize.x / Float(videoSize.width)
        assert(abs(ratio - worldSize.y/Float(videoSize.height)) < 0.01)
        self.positions = tjointpos.map { Vector3($0.x * ratio + topLeft.x, $0.y * ratio + topLeft.y, $0.z * ratio)}
        
    }
    
    
}


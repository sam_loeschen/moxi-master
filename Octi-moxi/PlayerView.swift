//
//  PlayerView.swift
//  Octi-moxi
//
//  Created by Octi on 9/1/17.
//  Copyright © 2017 Octi. All rights reserved.
//

import UIKit
import AVFoundation
import CoreAudio

class PlayerView: UIView {
    
    var _player: AVPlayer!
    
    var player: AVPlayer! {
        get {
            if _player == nil {
                _player = (layer as! AVPlayerLayer).player
            }
            return _player
        }
        set (player) {
            (layer as! AVPlayerLayer).player = player
            (layer as! AVPlayerLayer).videoGravity = .resizeAspectFill
        }
    }
    
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
}

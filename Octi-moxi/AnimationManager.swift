//
//  AnimationManager.swift
//  Octi-moxi
//
//  Copyright © 2017 Octi. All rights reserved.
//

import SceneKit
import SwiftyJSON
import AVFoundation
import UIKit
import RandomColorSwift

class AnimationManager {
    
    weak var controller : ViewController!

    var doDebug : Bool = true
    
    // joints is a list of all the frames of the video.
    // For example:
    // joints[10].positions is an array of Vector3 positions of all the joints at the 10th frame of the video
    // joints[10].positions[0] is the position of the root joint at the 10th frame of the video
    // joints[10].positions[0].x is the x coordinate of the position of the root joint at the 10th frame of the video
    // joints[10].raw.time is the time in seconds of the 10th frame of animation
    var joints : [Joints] = []
    
    // gestures is a list of all the frames of the video that have been marked as being part of a gesture
    // it contains a frameNumber (i.e. just an index into joints array) and a time (in seconds)
    var gestures : [Gesture] = []
    
    //Sam: geometry pieces
    var geometry : [Geometry] = []
    
    
    //Sam: last time is the stored time of the last frame, delta time is the difference
    //between last frame and current frame
    var lastTime = 0.0
    var deltaTime = 0.0
    
    // The main node that is attached to the scene's root node
    var node = SCNNode ()
    // A debug node that currently draws a sphere at every joint
    var debugNode = SCNNode ()
    
    
    // Bind to view controller
    func addToController (viewController : ViewController){
        controller = viewController
        controller = viewController
        
        controller.scnView.scene!.rootNode.addChildNode(node)
        controller.scnView.scene!.rootNode.addChildNode(debugNode)
        
        setup()
    }

    // Setup the scene graph
    func setup() {
        for _ in 0...14 {
            let sph = SCNSphere()
            sph.radius = 0.01
            let sphNode = SCNNode (geometry: sph)
            debugNode.addChildNode (sphNode)
        }
    }
    
    // Load in the joint data
    func load (jointFrames : [Joints], gestureFrames : [Gesture] ){
        joints = jointFrames
        gestures = gestureFrames
        
        //Sam: make colors for geometry
        let geoColors = randomColors(count: 15, hue: .random, luminosity: .light)
        
        //Sam: set up geometry to follow joints
        for i in 0..<40{
            let geo = Geometry(framePositions: joints[0].positions,
                               scene: controller.scnScene!, color: geoColors[i % geoColors.count])
            geometry.append(geo)
        }
    }

    
    // Called repeatedly by ViewController
    func doAnimation (time: Double) {
  
        let pastFrames = self.joints.filter { $0.raw.time <= time }
   
        //grab the most recent frame
        guard let frame = pastFrames.last else { return }
        
        
        if (self.doDebug) {
            guard frame.positions.count <= 15 else { return }
            for i in 0...(frame.positions.count-1) {
                debugNode.childNodes[i].position = SCNVector3(frame.positions[i])
            }
            
            //Sam: get deltaTime, update geometry
            deltaTime = time - lastTime
            lastTime = time
            for i in 0..<geometry.count {
                geometry[i].buildGeometry(framePositions: frame.positions)
                geometry[i].update(deltaTime: deltaTime)
            }
        } else {
            debugNode.isHidden = true
        }
    }
    
    // Clean up before loop
    func reset() {
        DispatchQueue.main.async { // do this on a little delay
            //reset any parameters you need here before looping video
            
            //Sam: reset last time
            self.lastTime = 0.0
            
        }
    }
}

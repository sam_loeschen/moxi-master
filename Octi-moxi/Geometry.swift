//
//  Geometry.swift
//  Octi-moxi
//
//  Created by Sam on 11/13/17.
//  Copyright © 2017 Octi. All rights reserved.
//

import Foundation
import SceneKit

struct Geometry{
    var jointIndexes : [Int] = []
    let jointRange : Int
    let tris : [Int] = [
        0, 2, 1,
        1, 2, 3
    ]
    var vertices : [SCNVector3] = []
    var triElement : SCNGeometryElement
    let scnScene : SCNScene
    let scnNode : SCNNode
    
    let lifeTimeDurationBias = 0.5 // < bias is short, > bias is long
    
    //min and max for short update
    let shortLifeMin = 0.4
    let shortLifeMax = 0.6
    
    //min and max for long update
    let longLifeMin = 0.7
    let longLifeMax = 1.0
    
    var currentLifeTime = 0.0
    var lifeTimer = 0.0
    
    let material : SCNMaterial
    let maxAlpha = 0.75
    let minAlpha = 0.0;
    
    init(framePositions: [Vector3], scene: SCNScene, color :UIColor){
        //set up node with scene
        scnScene = scene
        scnNode = SCNNode()
        scnScene.rootNode.addChildNode(scnNode)
        
        //set up material
        material = SCNMaterial()
        material.lightingModel = SCNMaterial.LightingModel.constant
        material.diffuse.contents = color
        
        //set joint range
        self.jointRange = framePositions.count
        
        //set up arrays that only need to be allocated once
        vertices = Array<SCNVector3>(repeating: SCNVector3(), count: 4)
        jointIndexes = Array<Int>(repeating: 0, count: 4)
        
        //create triangle data
        let indexData = NSData(bytes: tris, length: tris.count)
        triElement = SCNGeometryElement(data: indexData as Data,
                                        primitiveType: SCNGeometryPrimitiveType.triangles,
                                        primitiveCount: 2,
                                        bytesPerIndex: MemoryLayout<Int>.size)
        
        generateJointIndexes(range: jointRange)
        buildGeometry(framePositions: framePositions)
        
        //intialize update timer
        currentLifeTime = getNewLifeTime()
        lifeTimer = 0.0
    }
    mutating func generateJointIndexes(range: Int){
        //assign random joints
        for i in 0..<jointIndexes.count {
            jointIndexes[i] = Int(arc4random_uniform(UInt32(range)))
        }
    }
    mutating func buildGeometry(framePositions: [Vector3]){
        //set values in vertices array
        for i in 0...3{
            vertices[i] = SCNVector3.init(framePositions[jointIndexes[i]])
        }
        //create vertex data, build geometry source
        let vertexData = NSData(bytes: vertices, length:vertices.count  * MemoryLayout<SCNVector3>.size)
        let vertSource = SCNGeometrySource(data: vertexData as Data,
                                           semantic: SCNGeometrySource.Semantic.vertex,
                                           vectorCount: vertices.count,
                                           usesFloatComponents: true,
                                           componentsPerVector: 3,
                                           bytesPerComponent: MemoryLayout<Float>.size,
                                           dataOffset: 0,
                                           dataStride: MemoryLayout<SCNVector3>.size)
        //finally create geometry and assign to node
        scnNode.geometry = SCNGeometry(sources: [vertSource], elements: [triElement])
        scnNode.geometry?.firstMaterial = material
    }
    mutating func update(deltaTime: Double){
        lifeTimer += deltaTime / currentLifeTime
        
        //if timer has finished, reset timer, randomize joint positions, and change update speed
        if(lifeTimer >= 1){
            lifeTimer -= 1
            currentLifeTime = getNewLifeTime()
            generateJointIndexes(range: jointRange)
        }
        
        //update alpha, lerp over gaussian distribution
        let alpha = minAlpha + ((-0.5 * cos(2 * Double.pi * lifeTimer) + 0.5)) * (maxAlpha - minAlpha)
        scnNode.geometry?.firstMaterial?.transparency = CGFloat(alpha)
    }
    mutating func getNewLifeTime() -> Double{
        let chance = random0to1()
        if(chance > lifeTimeDurationBias){
            //do long update
            return longLifeMin + random0to1() * (longLifeMax - longLifeMin)
        }else{
            //do short update
            return shortLifeMin + random0to1() * (shortLifeMax - shortLifeMin)
        }
    }
    func random0to1() -> Double {
        return Double(arc4random()) / Double(UInt32.max)
    }
}

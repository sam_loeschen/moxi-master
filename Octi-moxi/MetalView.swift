//
//  MetalView.swift
//  Octi-moxi
//
//  Created by Sam on 11/8/17.
//  Copyright © 2017 Octi. All rights reserved.
//

import Foundation
import MetalKit
import CoreVideo

class MetalView: MTKView{
    
    //time stamp of video frame
    var inputTime : CFTimeInterval?
    
    //pixel buffer optained via readBuffer in ShaderInjector
    var pixelBuffer :CVPixelBuffer?{
        didSet{
            setNeedsDisplay()
        }
    }
    
    //this texture cache to convert from cv ixel buffer to metal texture
    var textureCache: CVMetalTextureCache?
    var commandQueue: MTLCommandQueue
    var computePipelineState: MTLComputePipelineState
    
    required init(coder: NSCoder) {
                
        // Get the default metal device
        let metalDevice = MTLCreateSystemDefaultDevice()!
        
        // Create a command queue
        self.commandQueue = metalDevice.makeCommandQueue()!
        
        // Create the metal library containing the shaders
        let bundle = Bundle.main
        let url = bundle.url(forResource: "default", withExtension: "metallib")
        let library = try! metalDevice.makeLibrary(filepath: url!.path)
        
        // Create a function with a specific name
        let function = library.makeFunction(name: "colorKernel")!
        
        // Create a compute pipeline with the above function
        self.computePipelineState = try! metalDevice.makeComputePipelineState(function: function)
        
        // Initialize the cache to convert the pixel buffer into a Metal texture
        var textCache: CVMetalTextureCache?
        if CVMetalTextureCacheCreate(kCFAllocatorDefault, nil, metalDevice, nil, &textCache) != kCVReturnSuccess {
            fatalError("Unable to allocate texture cache.")
        }
        else {
            self.textureCache = textCache
        }
        
        // Initialize super
        super.init(coder: coder)
        
        // Assign the metal device to this view
        self.device = metalDevice
        
        // Enable the current drawable texture read/write
        self.framebufferOnly = false
        
        // Disable drawable auto-resize
        self.autoResizeDrawable = false
        
        // Change drawing mode based on setNeedsDisplay()
        self.enableSetNeedsDisplay = true
        self.isPaused = true
        
        // Set the size of the drawable
        self.drawableSize = CGSize(width: 1920, height: 1080)
        
        //set up transform, rotate to be portrait, scale to fill. scale is a magic number at the moment
        let rotTransform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        let scaleTransform = CGAffineTransform(scaleX: 1.78, y: 1.78)
        self.transform = rotTransform.concatenating(scaleTransform)
        
        // Set the content mode to aspect fit
        self.contentMode = .scaleAspectFit
        


    }
    
    
    //this will be triggered by the display link in ShaderInjector
    override func draw(_ rect: CGRect){
        autoreleasepool{
            
            if(rect.width > 0 && rect.height > 0){
                self.render(self)
            }
        }
    }
    
    //this is the render method that converts the pixel buffer into a metal texture, runs the shader on it, and then sends it to the view
    private func render(_ view: MTKView) {
        
        //pixel buffer must exist
        guard let pixelBuffer = self.pixelBuffer else { return }
        
        //Get width and height of pixel buffer
        let width = CVPixelBufferGetWidth(pixelBuffer)
        let height = CVPixelBufferGetHeight(pixelBuffer)
        
        // Converts the pixel buffer in a Metal texture
        var cvTextureOut: CVMetalTexture?
        CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault, self.textureCache!, pixelBuffer, nil, .bgra8Unorm, width, height, 0, &cvTextureOut)
        guard let cvTexture = cvTextureOut, let inputTexture = CVMetalTextureGetTexture(cvTexture) else {
            fatalError("Failed to create metal texture")
        }
        //make sure we have a drawable
        guard let drawable: CAMetalDrawable = self.currentDrawable else { return }
        
        //create command buffer
        let commandBuffer = commandQueue.makeCommandBuffer()!
        
        //create compute command encoder
        let computeCommandEncoder = commandBuffer.makeComputeCommandEncoder()!
        
        //set the compute pipline state for command encoder
        computeCommandEncoder.setComputePipelineState(computePipelineState)
        
        //set input and output textures for the shader
        computeCommandEncoder.setTexture(inputTexture, index: 0)
        computeCommandEncoder.setTexture(drawable.texture, index: 1)
        
        //convert time into a metal buffer
        var time = Float(self.inputTime!)
        computeCommandEncoder.setBytes(&time, length: MemoryLayout<Float>.size, index: 0)
        
        //Encode a threadgroup's execution of a compute function
        computeCommandEncoder.dispatchThreadgroups(inputTexture.threadGroups(), threadsPerThreadgroup: inputTexture.threadGroupCount())
        
        //end encoding
        computeCommandEncoder.endEncoding()
        
        //set the drawable to render
        commandBuffer.present(drawable)
        
        //commit command buffer for execution
        commandBuffer.commit()

    }
}
extension MTLTexture {
    func threadGroupCount() -> MTLSize {
        return MTLSizeMake(8, 8, 1)
    }
    func threadGroups() -> MTLSize {
        let groupCount = threadGroupCount()
        return MTLSizeMake(Int(self.width) / groupCount.width, Int(self.height) / groupCount.height, 1)
    }
}


